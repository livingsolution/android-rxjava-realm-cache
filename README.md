IMPORTANT NOTE:

When I wrote this I thought it might be a good idea, but today I think it is not.

There are better ways to build this architecture (search: reactive clean architecture), and I stopped using Realm a while ago.

I leave the repo for future self-reference but I don't recommend to anyone to do what I do here.

Otherwise, you have been warned, now have fun.



# android-rxjava-realm-cache
Example of how to use Realm to cache responses and combine them later

##### API KEY

Create the `apikey.xml` file in the `res/values` and set the api key value there
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="api_key">your key</string>
</resources>
```

